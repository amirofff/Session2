package sematec.sematecsaturdays;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    TextView studentName;
    Button btnShow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        studentName = (TextView) findViewById(R.id.studentName);

        btnShow = (Button) findViewById(R.id.btnShow);

        studentName.setText("Amirhossein");

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentName.setText("Hello Worlddddddd");
                Toast.makeText(SecondActivity.this, "Clicked on BTN SHOW", Toast.LENGTH_LONG).show();
            }
        });


        btnShow.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(SecondActivity.this, "onLong Click", Toast.LENGTH_SHORT).show();


                return false;
            }
        });



        studentName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnShow.setText("Alireza");
            }
        });



    }
}
